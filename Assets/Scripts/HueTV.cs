﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.Experimental.Networking;

public class HueTV : MonoBehaviour {

	bool HueOn = true;
	// Use this for initialization
	void Start(){
	}

	IEnumerator TurnOnHue() {
		Debug.Log ("HueTv Off");
		UnityWebRequest turnOn = UnityWebRequest.Get ("http://10.134.15.103:3005/api/allLights/on/0");
		yield return turnOn.Send ();
	}

	IEnumerator TurnOffHue() {
		Debug.Log ("HueTv On");
		UnityWebRequest turnOff = UnityWebRequest.Get ("http://10.134.15.103:3005/api/allLights/off/0");
		yield return turnOff.Send ();
	}

	void OnMouseDown() {
		if (HueOn == false) {
			StartCoroutine(TurnOnHue());
			HueOn = true;
		} else {
			StartCoroutine(TurnOffHue());
			HueOn = false;
		}
	}
}
