﻿using UnityEngine;
using System.Collections;
using UnityEngine.Experimental.Networking;
using System;

public class HueButton : MonoBehaviour {
	
	private string color;
	private int hueNb;
	private Light lightHue;

	HueButton() {
		this.hueNb = 1;
		this.color = "red";

	}

	// Use this for initialization
	void Start () {
		this.lightHue = this.GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void setParameters(int hueNb, string color) {
		this.hueNb = hueNb;
		this.color = color;
	}

	void ChangeLight(Color color) {
		this.lightHue.color = color;
		this.lightHue.range = 1;
		this.lightHue.intensity = 0.1f;
		this.lightHue.type = LightType.Point;
		this.lightHue.bounceIntensity = 0.1f;
	}

	void TurnOffLight() {
		this.lightHue.intensity = 0;
	}

	IEnumerator TurnOnHue() {
		string url = String.Format ("{0}lights/color/{1}/{2}", ApiConfig.API_BASE_URL, this.hueNb, this.color);
		UnityWebRequest turnOn = UnityWebRequest.Get (url);

		yield return turnOn.Send ();
	}

	IEnumerator TurnOffHue() {
		string url = String.Format ("{0}lights/power/{1}/off", ApiConfig.API_BASE_URL, this.hueNb);
		UnityWebRequest turnOff = UnityWebRequest.Get (url);

		yield return turnOff.Send ();
	}

	// Hue 1 à gauche

	public void OnClickTurnOffHue1() {
		this.hueNb = 1;
		StartCoroutine (TurnOffHue ());
		this.TurnOffLight ();
	}

	public void OnClickHue1Red() {
		this.setParameters (1, "red");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 0, 0));
	}

	public void OnClickHue1Blue() {
		this.setParameters (1, "blue");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 0, 255));
	}

	public void OnClickHue1Cyan() {
		this.setParameters (1, "cyan");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 255, 255));
	}

	public void OnClickHue1Green() {
		this.setParameters (1, "green");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 255, 0));
	}

	public void OnClickHue1Orange() {
		this.setParameters (1, "orange");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 165, 0));
	}

	public void OnClickHue1Yellow() {
		this.setParameters (1, "yellow");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 255, 0));
	}

	public void OnClickHue1Purple() {
		this.setParameters (1, "purple");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (128, 0, 128));
	}

	public void OnClickHue1Teal() {
		this.setParameters (1, "teal");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 128, 128));
	}


	// Hue 0 à droite

	public void OnClickTurnOffHue0() {
		this.hueNb = 2;
		StartCoroutine (TurnOffHue ());
		this.TurnOffLight ();
	}

	public void OnClickHue0Red() {
		this.setParameters (2, "red");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 0, 0));
	}

	public void OnClickHue0Blue() {
		this.setParameters (2, "blue");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 0, 255));
	}

	public void OnClickHue0Cyan() {
		this.setParameters (2, "cyan");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 255, 255));
	}

	public void OnClickHue0Green() {
		this.setParameters (2, "green");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 255, 0));
	}

	public void OnClickHue0Orange() {
		this.setParameters (2, "orange");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 165, 0));
	}

	public void OnClickHue0Yellow() {
		this.setParameters (2, "yellow");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (255, 255, 0));
	}

	public void OnClickHue0Purple() {
		this.setParameters (2, "purple");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (128, 0, 128));
	}

	public void OnClickHue0Teal() {
		this.setParameters (2, "teal");
		StartCoroutine(TurnOnHue());
		this.ChangeLight(new Color (0, 128, 128));
	}

}
