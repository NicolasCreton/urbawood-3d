﻿using UnityEngine;
using System.Collections;

public class MenuPlanCuisineHide : MonoBehaviour {

	private Canvas canvas;

	void Start () {
		this.canvas = GameObject.Find ("Menu Plan Cuisine").GetComponent<Canvas> ();
		this.canvas.enabled = false;
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnClickHideMenu() {
		this.canvas.enabled = !this.enabled;
	}

	void OnMouseDown() {
		if (!this.canvas.enabled) {
			this.canvas.enabled = true;
		}
	}
}
