﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlanCuisine : MonoBehaviour {

	private float height;
	private Vector3 target;
	private float speed;
	private Text textUI;

	private const int MIN_HEIGHT = -20;
	private const int MAX_HEIGHT = 20;

	// Use this for initialization
	void Start () {
		this.height = transform.position.y;
		this.target = transform.position;
		this.speed = 0.05f;

		this.textUI = GameObject.Find("planHeight").GetComponent<Text>();
		this.textUI.text = Mathf.Round (this.height * 10).ToString();
	}

	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, this.target, step);
	}

	public void OnClickUp() {
		if (this.target.y * 100 < MAX_HEIGHT) {
			this.target += new Vector3 (0, 0.01f, 0);
			this.textUI.text = Mathf.Round (this.target.y * 100).ToString();
		}
	}

	public void OnClickDown() {
		
		if (this.target.y * 100 > MIN_HEIGHT) {
			this.target -= new Vector3 (0, 0.01f, 0);
			this.textUI.text = Mathf.Round (this.target.y * 100).ToString();
		}
	}
}
