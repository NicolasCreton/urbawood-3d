﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.Experimental.Networking;

public class PowerTV : MonoBehaviour {

	bool tvOn = false;
	// Use this for initialization
	void Start () {
	
	}
	
	IEnumerator PowerOnTv() {
		Debug.Log ("PowerOnTv");
		UnityWebRequest turnOn = UnityWebRequest.Get ("http://10.134.15.103:3005/api/bravia/power/on");
		tvOn = true;
		yield return turnOn.Send ();
	}

	IEnumerator PowerOffTv() {
		Debug.Log ("PowerOffTv");
		UnityWebRequest turnOff = UnityWebRequest.Get ("http://10.134.15.103:3005/api/bravia/power/off");
		tvOn = false;
		yield return turnOff.Send ();
	}
		
	/*IEnumerator SetChannel(int channel) {
		if (tvOn == false) {
			UnityWebRequest turnOn = UnityWebRequest.Get ("http://10.134.15.103:3005/api/bravia/power/on");
			tvOn = true;
			yield return turnOn.Send ();
		}
		Debug.Log ("Set Channel " + channel);
		UnityWebRequest requestChannel = UnityWebRequest.Get ("http://10.134.15.103:3005/api/tv/chanel/"+channel);
		yield return requestChannel.Send ();
	}*/

	void OnMouseDown() {
		Debug.Log ("OnClick");
		if (tvOn == false) {
			StartCoroutine(PowerOnTv());
		} else {
			StartCoroutine(PowerOffTv());
		}
	}
}
