﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Experimental.Networking;

public class ChannelButton : MonoBehaviour {

	private int channel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator PowerOnTv() {
		String url = String.Format ("{0}bravia/power/on", ApiConfig.API_BASE_URL);
		UnityWebRequest turnOn = UnityWebRequest.Get (url);
		yield return turnOn.Send ();
	}

	IEnumerator PowerOffTv() {
		String url = String.Format ("{0}bravia/power/off", ApiConfig.API_BASE_URL);
		UnityWebRequest turnOff = UnityWebRequest.Get (url);
		yield return turnOff.Send ();
	}

	IEnumerator ChangeChannel() {
		string url = String.Format ("{0}tv/channel/{1}", ApiConfig.API_BASE_URL, this.channel);
		UnityWebRequest changeChannel = UnityWebRequest.Get (url);
		yield return changeChannel.Send ();
	}

	public void OnClickPowerOffTV() {
		StartCoroutine (PowerOffTv ());
	}

	public void OnClickPowerOnTV() {
		StartCoroutine (PowerOnTv ());
	}

	public void OnClickChannel1() {
		this.channel = 1;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel2() {
		this.channel = 2;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel6() {
		this.channel = 6;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel7() {
		this.channel = 7;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel8() {
		this.channel = 8;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel9() {
		this.channel = 9;
		StartCoroutine (ChangeChannel ());
	}

	public void OnClickChannel15() {
		this.channel = 15;
		StartCoroutine (ChangeChannel ());
	}

}
