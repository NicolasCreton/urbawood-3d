﻿using UnityEngine;
using System.Collections;

public class MenuHide1 : MonoBehaviour {

	// Use this for initialization

	private Canvas canvas;

	void Start () {
		this.canvas = GameObject.Find ("Menu Hue 1").GetComponent<Canvas> ();
		this.canvas.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClickHideMenu() {
		this.canvas.enabled = !this.enabled;
	}

	void OnMouseDown() {
		Canvas menu1 = GameObject.Find ("Menu Hue 2").GetComponent<Canvas> ();
		Canvas menu2 = GameObject.Find ("Menu TV").GetComponent<Canvas> ();

		if (!this.canvas.enabled && !menu1.enabled && !menu2.enabled) {
			this.canvas.enabled = true;
		}
	}
}
