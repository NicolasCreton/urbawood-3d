﻿using UnityEngine;
using System.Collections;

public class MenuTvHide : MonoBehaviour {

	private Canvas canvas;

	// Use this for initialization
	void Start () {
		this.canvas = GameObject.Find ("Menu TV").GetComponent<Canvas> ();
		this.canvas.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClickHideMenuTV() {
		this.canvas.enabled = !this.enabled;
	}

	void OnMouseDown() {
		Canvas menu1 = GameObject.Find ("Menu Hue 1").GetComponent<Canvas> ();
		Canvas menu2 = GameObject.Find ("Menu Hue 2").GetComponent<Canvas> ();

		if (!this.canvas.enabled && !menu1.enabled && !menu2.enabled) {
			this.canvas.enabled = true;
		}
	}


}
