﻿using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine;

public class HandleGyro : MonoBehaviour
{
	public void activateGyro(bool check)
	{
		if (check == true) {
			Input.gyro.enabled = true;
		} else {
			Input.gyro.enabled = false;
		}
	}
}


