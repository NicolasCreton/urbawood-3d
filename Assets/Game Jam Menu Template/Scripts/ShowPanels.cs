﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ShowPanels : MonoBehaviour {

	public GameObject optionsPanel;							//Store a reference to the Game Object OptionsPanel 
	public GameObject optionsTint;							//Store a reference to the Game Object OptionsTint 
	public GameObject menuPanel;							//Store a reference to the Game Object MenuPanel 
	public GameObject pausePanel;							//Store a reference to the Game Object PausePanel 
	private GameObject joystickButton;


	//Call this function to activate and display the Options panel during the main menu
	public void ShowOptionsPanel()
	{
		optionsPanel.SetActive(true);
		optionsTint.SetActive(true);
		GameObject gameObjectGyro = optionsPanel.transform.Find ("Toggle").gameObject;
		if (gameObjectGyro != null) {
			Toggle toggleGyro = gameObjectGyro.GetComponent<Toggle> ();
			if (Input.gyro.enabled == true) {
				toggleGyro.isOn = true;
			} else {
				toggleGyro.isOn = false;
			}

		}
	}

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanel()
	{
		optionsPanel.SetActive(false);
		optionsTint.SetActive(false);
	}

	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
		menuPanel.SetActive (true);
	}

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
		menuPanel.SetActive (false);
	}
	
	//Call this function to activate and display the Pause panel during game play
	public void ShowPausePanel()
	{
		pausePanel.SetActive (true);
		optionsTint.SetActive(true);
		//Hide the joysticks when the pause menu is on
		joystickButton = GameObject.Find ("MobileSingleStickControl");
		if (joystickButton != null) {
			joystickButton.GetComponent<Canvas> ().enabled = false;
		}
		//Handle the Gyro Toggle
		GameObject gameObjectGyro = pausePanel.transform.Find ("Toggle").gameObject;
		if (gameObjectGyro != null) {
			Toggle toggleGyro = gameObjectGyro.GetComponent<Toggle> ();
			if (Input.gyro.enabled == true) {
				toggleGyro.isOn = true;
			} else {
				toggleGyro.isOn = false;
			}

		}

	}

	//Call this function to deactivate and hide the Pause panel during game play
	public void HidePausePanel()
	{
		pausePanel.SetActive (false);
		optionsTint.SetActive(false);

		//Show the joysticks when the pause menu is off
		if (joystickButton != null) {
			joystickButton.GetComponent<Canvas> ().enabled = true;
		}

	}
}
